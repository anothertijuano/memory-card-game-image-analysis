var completedCards = []
var cardsOnTurn = []


function comparison() {
  //Esta es la función que llama al sistema de comparación
  return(false);
}


function btnEnabler(btnID, conditionID){
  var btn = document.getElementById(btnID)
  var condition = document.getElementById(conditionID)
  if(condition.value){
    btn.disabled = false
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
   
async function notEqual() {
  await sleep(1000);
  flipAll()
}


function flipCard(backId, frontId){
  if(cardsOnTurn.indexOf(frontId) == -1) {
  console.log(cardsOnTurn.length)
    
    if(cardsOnTurn.length < 1){
      upside(frontId, backId)
    }
    else {
      var equal = comparison();
      console.log(equal);
      if(equal){
        completedCards.push(cardsOnTurn[0])
        completedCards.push(cardsOnTurn[1])
      }
      else {
        upside(frontId, backId)
        notEqual()
        cardsOnTurn = []
      }
    }

  }
}

function upside(frontId, backId) {
  document.getElementById(backId).style.display = 'none'
  document.getElementById(frontId).style.display = 'block'
  cardsOnTurn.push(frontId)
}

function downside(frontId, backId) {
  document.getElementById(frontId).style.display = 'none'
  document.getElementById(backId).style.display = 'block'
}


function flipAll() {
  var frontSides = document.getElementsByClassName("cardFront")
  var backSides =  document.getElementsByClassName("cardBack")

  for(var i = 0; i<frontSides.length; i++) {
    if ( completedCards.indexOf( frontSides[i] ) == -1 ) {
      downside(frontSides[i].id, backSides[i].id) 
    }
  }
  cardsOnTurn = []
}
