var completedCards = []
var cardsOnTurn = []
var imgComp = []
var res

function comparison(imgComp1, imgComp2) {
  console.log("func() front")
  var patron = "http://127.0.0.1:5000"
  var imgs = []
  imgs.push(imgComp1.replace(patron, "app/statics"))
  imgs.push(imgComp2.replace(patron, "app/statics"))
    fetch(`/compare`, {
    method: "POST",
    body: JSON.stringify(imgs),
    cache: "no-cache",
    headers: new Headers({
      "content-type": "application/json"
    })
    }).then(function(response) {
        if (response.status !== 200) {
            console.log(`Looks like there was a problem. Status code: ${response.status}`);
            return;
        }
        response.json().then(function(data) {
            //console.log(data['message']);
            if(data['message'] === 'True')
              res = true
            else
              res = false
        });
    }).catch(function(error) {
        console.log("Fetch error: " + error);
    });
  return res
}


function btnEnabler(btnID, conditionID){
  var btn = document.getElementById(btnID)
  var condition = document.getElementById(conditionID)
  if(condition.value){
    btn.disabled = false
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
   
async function notEqual() {
  await sleep(1000);
  flipAll()
}


async function flipCard(backId, frontId){
  if(cardsOnTurn.indexOf(frontId) == -1) {
  console.log(cardsOnTurn.length)
    
    if(cardsOnTurn.length < 1){
      upside(frontId, backId)
      dir = document.getElementById(frontId).src
      imgComp.push(dir)
    }
    else {
      dir = document.getElementById(frontId).src
      imgComp.push(dir)
      var equal = await comparison(imgComp[0], imgComp[1]);
      console.log(equal)
      if(equal){
        completedCards.push(cardsOnTurn[0])
        completedCards.push(cardsOnTurn[1])
      }
      else {
        upside(frontId, backId)
        notEqual()
        
      }
      console.log(equal)
      cardsOnTurn = []
      imgComp = []
    }
  }
}

function upside(frontId, backId) {
  document.getElementById(backId).style.display = 'none'
  document.getElementById(frontId).style.display = 'block'
  cardsOnTurn.push(frontId)
}

function downside(frontId, backId) {
  document.getElementById(frontId).style.display = 'none'
  document.getElementById(backId).style.display = 'block'
}


function flipAll() {
  var frontSides = document.getElementsByClassName("cardFront")
  var backSides =  document.getElementsByClassName("cardBack")

  for(var i = 0; i<frontSides.length; i++) {
    if ( completedCards.indexOf( frontSides[i] ) == -1 ) {
      downside(frontSides[i].id, backSides[i].id) 
    }
  }
  cardsOnTurn = []
}

function func() {
    var xml = new XMLHttpRequest();
    xml.open("POST", "{{url_for(func.func)}}", true);
    xml.setRequestHeader("Content-type", 
        "application/x-www-fornm-urlencoded");
    xml.onload = function(){
        var dataReply = JSON.parse(this.responseText)
        alert(dataReply)
    };

    dataSend = JSON.stringify({
        'somedata':'somedata'
    });
};
